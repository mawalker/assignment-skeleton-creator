package com.righteouscoder.skeletoncreator.application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handles the Programming Language specific processing for Assignments.
 * <p>
 * List of language support is not 100% complete, but covers the 3 most common inline comment
 * styles: "//", "#", and "<\!--".
 * matching lines with the following regex: "(.*)(//|#|<\!--).*ASSIGNMENT TO-DO)(.*)"
 * <p>
 * File Types handled need to be specifically listed.
 * <p>
 * Current File Extension Types handled: {@value #HandledExtensions}
 *
 * @author Michael A. Walker
 * @version 2.0
 */
class CodeSyntax {

    final static String ASSIGNMENT_TODO = "ASSIGNMENT TO-DO";

    /**
     * This pattern allows usage with the following languages:
     * C, C++, C#, Java, JavaScript, Objective-C, PHP, etc.
     */
    private final static Pattern codeCommentStart =
            Pattern.compile(".*//.?" + ASSIGNMENT_TODO + "(.*)");

    /**
     * This pattern allows usage with the following languages:
     * XML
     */
    private final static Pattern xmlCommentStart =
            Pattern.compile(".*<!--.?\" + ASSIGNMENT_TODO + \"(.*)");

    /**
     * This pattern allows usage with the following languages:
     * Bourne & other UNIX shells, Perl, Python, etc.
     */
    private final static Pattern bashCommentStart =
            Pattern.compile(".*#.?\" + ASSIGNMENT_TODO + \"(.*)");

    /**
     * Extensions that the program should parse
     * <p>
     * Used for PathMatcher syntax for @extensionMatcher, needs to be comma-separated values.
     */
    final static String HandledExtensions =
            "java,xnl,h,c,cpp,py,js,pl,php,sh,cs,m";

    /**
     * Matcher for the different file extensions to be parsed for removing of assignment sections.
     */
    private final static PathMatcher extensionMatcher = FileSystems.getDefault()
            .getPathMatcher("glob:**.{" + HandledExtensions + "}");

    /**
     * Check if file's type should be processed for removal of assignment sections during copying
     *
     * @param inputPath Path of the file to be copied.
     * @return true if this file should be processed, false otherwise.
     */
    boolean isFileTypeProcessed(Path inputPath) {
        return extensionMatcher.matches(inputPath);
    }

    /**
     * This method copies an input file to the output file location but removes lines between
     * matching pairs of {@value #ASSIGNMENT_TODO} comments.
     * </p>
     * Comments are inline comments that start with "//", "#", or "<!--" (more are possible to be
     * added in future, but these 3 cover almost all modern languages.)
     *
     * @param sourceFile      source File
     * @param destinationFile destination File
     */
    @SuppressWarnings("ThrowFromFinallyBlock")
    void processAndCopyFile(File sourceFile, File destinationFile) throws IOException {

        /*
         * Precondition checking
         */
        if (!sourceFile.exists()) {
            System.err.println("ERROR: No such file exist at the path: "
                    + sourceFile.getPath());
            return;
        }
        if (sourceFile.isDirectory()) {
            System.err.println("ERROR: There is a directory, not a File at the path: " +
                    sourceFile.getPath());
            return;
        }
        if (!sourceFile.isFile()) {
            System.err.println("ERROR: The resource(if any) is NOT a File at the path: " +
                    sourceFile.getPath());
            return;
        }

        BufferedReader br = null;
        PrintWriter pw = null;

        try {
            br = new BufferedReader(new FileReader(sourceFile));
            pw = new PrintWriter(new FileWriter(destinationFile));

            String line;

            // loop over each line in the original file
            while ((line = br.readLine()) != null) {

                // TODO maybe clean up to only run the one matcher needed, not all 3.

                // see if this line matches the pattern of 'codeCommentStart'
                Matcher codeCommentMatcher = codeCommentStart.matcher(line);
                Matcher xmlCommentMatcher = xmlCommentStart.matcher(line);
                Matcher pythonCommentMatcher = bashCommentStart.matcher(line);

                checkPatternMatch(codeCommentMatcher);
                checkPatternMatch(xmlCommentMatcher);
                checkPatternMatch(pythonCommentMatcher);

                if (isKeeping && !skipNext) {
                    pw.println(line);
                }
                skipNext = false;
            }
        } finally {
            if (br != null) {
                br.close();
            }
            if (pw != null) {
                pw.close();
            }
        }

    }

    private boolean isKeeping = true;
    private boolean skipNext = false;
    private String currentDeletionTag = "";

    /**
     * This is a helper method to check if each matcher matches the target line, and then sets
     * processing flags appropriately if needed.
     *
     * @param matcher the match to be checked and processed.
     */
    private void checkPatternMatch(final Matcher matcher) {
        if (matcher.matches()) {
            String foundDeletionTag = matcher.group(1);
            if (!isKeeping) {
                if (foundDeletionTag.equals(currentDeletionTag)) {
                    isKeeping = true;
                    currentDeletionTag = "";
                    skipNext = true;
                } else {
                    System.err.println("Expected Assignment tag: " + currentDeletionTag + " " +
                            "and found Tag: " + foundDeletionTag);
                    System.exit(1);
                }
            } else {
                isKeeping = false;
                currentDeletionTag = foundDeletionTag;
            }
        }
    }

}
