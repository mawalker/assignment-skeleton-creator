package com.righteouscoder.skeletoncreator.application;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * <p>
 * Simple program to facilitate automatic creation of "Skeleton File(s)" for assignments, via use
 * of special 'Flag' comment pairs used for parsing during copying of the original files to a new
 * location.
 * <p>
 * Handles the following extensions: {@value CodeSyntax#HandledExtensions}
 * <p>
 * Inline comment styles supported for : "//", "#", and "<\!--".
 * <p>
 * To create a "skeleton" version of your assignment, you either select an individual input file
 * and a corresponding output file(will be overwritten) or you select an input directory and an
 * (already existing) output directory(that will be purged unless a command line option is given).
 * In either case, the file or directories & files will be copied exactly from the input location to
 * the output location. The only exception is files with ending extensions that this program can
 * properly parse (Listed above). For those files that can be parsed, they will be parsed line by
 * line and special 'Flag' in-line comments will be used to flag sections of the file for deletion
 * in the new copy. (original file is untouched).
 * <p>
 * The 'Flag' in-line comments need to start with the appropriate comment characters then have
 * {@value CodeSyntax#ASSIGNMENT_TODO} and some additional text to uniquely identify each pair of
 * comments. The 'Flag' comment lines, including all lines between them, will be deleted from the
 * newly created copy of the file/directory.
 *
 * @author Michael A. Walker
 * @version 2.0
 */
public class Main {

    /**
     * DEBUG TEST flag... because Android Studio doesn't like this project...
     * <p>
     * (yes I know this is a dirty hack... but I don't want to install full IDEA IDE right now..)
     * <p>
     * And for now it works fine this way... should be cleaned up in the future.
     */
    private final static boolean DEBUG_TEST = false;

    /**
     * Main method that is ran from command line.
     *
     * @param args command line arguments.
     */
    public static void main(String[] args) {
        if (!DEBUG_TEST) {
            Main main = new Main();
            main.parseCommandLineArgs(args);
        } else {
            mainTest();
        }
    }

    /**
     * Helper method to facilitate testing when IDEA/Android Studio doesn't want to import junit
     */
    private static void mainTest() {
        Main main = new Main();
        System.out.println(System.getProperty("user.dir"));

        String input = System.getProperty("user.dir") + "/test-input/";
        String output = System.getProperty("user.dir") + "/test-output/";
        String[] processArgs = new String[]{input, output};

        main.parseCommandLineArgs(processArgs);
    }

    // input strings from command line
    private String fileLocationIn = null;
    private String fileLocationOut = null;

    // Files from input strings.
    private File inputLocationFile = null;
    private File outputLocationFile = null;

    // boolean flag if purging of directory should/shouldn't exist.
    private boolean purgeDirectory = true;

    /**
     * Parse the command line arguments passed the program, and then kick off processing.
     *
     * @param args the command line arguments passed to the execution of this program.
     */
    private void parseCommandLineArgs(String[] args) {

        // if at least 2 args, pull out first and second for input/output locations.
        if (args.length >= 2) {
            fileLocationIn = args[0];
            fileLocationOut = args[1];
        } else {
            System.err.println("Need to have at least 2 arguments: " +
                    "<Input File/Directory Location> " +
                    "<Output File/Directory Location> " +
                    "<(Optional)Purge Output Directory: (Default)'true' or 'false' >");
            System.exit(1);
        }

        // if 3rd arg, extract purgeDirectory value.
        if (args.length > 3) {
            try {
                purgeDirectory = Boolean.getBoolean(args[2]);
            } catch (Exception ex) {
                System.err.println("Error parsing 3rd parameter. " +
                        "Only 'true' or 'false' accepted.");
                System.exit(1);
            }
        }

        inputLocationFile = new File(fileLocationIn);
        outputLocationFile = new File(fileLocationOut);

        // process the arguments and do the logical work of the program.'
        process();
    }

    /**
     * Actually process the input command arguments after parsing.
     * <p>
     * Copying a single file to a location, or copying the contents of a directory into another
     * (already existing) directly.
     */
    private void process() {

        boolean exists = inputLocationFile.exists();      // Check if the file exists
        boolean isDirectory = inputLocationFile.isDirectory(); // Check if it's a directory
        boolean isFile = inputLocationFile.isFile();      // Check if it's a regular file

        if (!exists) {
            // input location doesn't exist
            System.err.println("Input file/directory does not exist: " + inputLocationFile.getPath());
            System.exit(1);
        } else if (isFile) {
            // copy a single file.
            try {
                CodeSyntax mCodeSyntax = new CodeSyntax();
                mCodeSyntax.processAndCopyFile(inputLocationFile, outputLocationFile);
            } catch (IOException ex) {
                System.err.println("ERROR creating assignment version of file located at: "
                        + inputLocationFile.getPath());
                System.err.println("ERROR message: " + ex.getMessage());
                System.exit(1);
            }
        } else if (isDirectory) {
            /*
             * Logical Error checking before starting to copy directory contents.
             */
            if (!outputLocationFile.exists()) {
                System.err.println("Output directory does not exist. " +
                        "It must exist for directory copying.");
                System.exit(1);
            } else if (!outputLocationFile.isDirectory()) {
                System.err.println("Output location is not a directory.");
                System.exit(1);
            }

            // if purgeDirectory == true, do the purge.
            if (purgeDirectory) {
                purgeDirectory(outputLocationFile);
            }

            // copy the directory recursively.
            try {
                copyDirectory(Paths.get(fileLocationIn), Paths.get(fileLocationOut));
            } catch (IOException e) {
                System.err.println("Error running the Assignment Skeleton Creator: Message: "
                        + e.getMessage());
                System.exit(1);
            }
        } else {
            System.err.println("Error input location was neither file nor directory" +
                    "( but it -did- exist... (No clue why this would happen... sim-link?))");
            System.exit(1);
        }
    }

    /**
     * Purge the contents of a directory without deleting the directory itself.
     *
     * @param dir directory to be purged.
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void purgeDirectory(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) purgeDirectory(file);
                file.delete();
            }
        }
    }

    /**
     * Copy a directory to the output directory, processing any known file types during the
     * process to remove marked assignment sections.
     *
     * @param sourcePath path of directory to copy.
     * @param targetPath path of output directory
     * @throws IOException
     */
    private void copyDirectory(final Path sourcePath, final Path targetPath) throws IOException {
        Files.walkFileTree(sourcePath, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult preVisitDirectory(final Path dir,
                                                             final BasicFileAttributes attrs)
                            throws IOException {
                        Files.createDirectories(targetPath.resolve(sourcePath
                                .relativize(dir)));
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFile(final Path file,
                                                     final BasicFileAttributes attrs) throws IOException {

                        File inputFile = new File(file.toUri());
                        File outputFile = new File(targetPath.resolve(sourcePath.relativize(file)).toUri());

                        CodeSyntax codeChecker = new CodeSyntax();

                        // Check if this file type (based off end extension of file path) is processed.
                        if (codeChecker.isFileTypeProcessed(file)) {
                            // This file type is processed during copying.
                            codeChecker.processAndCopyFile(inputFile,
                                    outputFile);
                        } else {
                            // all other file types
                            Files.copy(file,
                                    targetPath.resolve(sourcePath.relativize(file)),
                                    StandardCopyOption.REPLACE_EXISTING);
                        }
                        // continue walking
                        return FileVisitResult.CONTINUE;

                    }
                }

        );
    }
}
