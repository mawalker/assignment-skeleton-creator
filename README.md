# Simple program to facilitate automatic creation of "Skeleton File(s)" for assignments

Uses of special 'Flag' comment pairs for parsing during copying of the original files to a new location.

---

#### Handles the following extensions: "java,xnl,h,c,cpp,py,js,pl,php,sh,cs,m"

#### Inline comment styles supported for:  "//", "#", and "<\!- -".

---

## To create a "skeleton" version of your assignment
You either select an individual input file and a corresponding output file(will be overwritten) or you select an input directory and an (already existing) output directory(that will be purged unless a command line option is given). In either case, the file or directories & files will be copied exactly from the input location to the output location. The only exception is files with ending extensions that this program can properly parse (Listed above). For those files that can be parsed, they will be parsed line by line and special 'Flag' in-line comments will be used to flag sections of the file for deletion in the new copy. (original file is untouched). 

#### The 'Flag' in-line comments

The 'Flag' in-line comments need to start with the appropriate comment characters then have "**ASSIGNMENT TO-DO**" and some additional text to uniquely identify each pair of comments. The 'Flag' comment lines, including all lines between them, will be deleted from the newly created copy of the file/directory.

-----------

##### Example Input (Java): 

line 1. System.out.println("line 1");

line 2. System.out.println("line 2");

// Put your work here for lines 3 throguh 5:

--blank line--

// ASSIGNMENT TO-DO Several Lines of output

line 3. System.out.println("line 3");

line 4. System.out.println("line 4");

line 5. System.out.println("line 5");

// ASSIGNMENT TO-DO Several Lines of output

--blank line--

line 6. System.out.println("line 6");

##### Example Output 

line 1. System.out.println("line 1");

line 2. System.out.println("line 2");

// Put your work here for lines 3 throguh 5:

--blank line--

--blank line--

line 6. System.out.println("line 6");

---------

#### Command Line arguments:

Need to have at least 2 arguments: < Input File/Directory Location > < Output File/Directory Location > < (Optional)Purge Output Directory: (Default)'true' or 'false' >


--------

 
##### author Michael A. Walker
##### version 2.0