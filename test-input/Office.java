package mooc.vandy.java4android.buildings.logic;

/**
 * This is the office class file, it is a subclass of Building.
 */
public class Office
       extends Building {

    // TODO - Put your code here.
    // ASSIGNMENT TO-DO: DELETE TO END OF CLASS
    
    /**
     * Name of the business in the office.
     */
    private String mBusinessName;

    /**
     * Number of parking spaces.
     */
    private int mParkingSpaces;

    /**
     * Total count of the offices.
     */
    private static int sTotalOffices = 0;

    /**
     * Constructor initializes the fields.
     */
    public Office(int length,
                  int width,
                  int lotLength,
                  int lotWidth) {
        super(length,
              width,
              lotLength,
              lotWidth);
        mBusinessName = null;
        mParkingSpaces = 0;
        sTotalOffices++;
    }
	
    /**
     * Constructor initializes the fields.
     */
    public Office(int length,
                  int width,
                  int lotLength,
                  int lotWidth,
                  String businessName) {
        super(length,
              width,
              lotLength,
              lotWidth);
        mBusinessName = businessName;
        mParkingSpaces = 0;
        sTotalOffices++;
    }

    /**
     * Constructor initializes the fields.
     */
    public Office(int length,
                  int width,
                  int lotLength,
                  int lotWidth,
                  String businessName,
                  int parkingSpaces) {
        super(length,
              width,
              lotLength,
              lotWidth);
        mBusinessName = businessName;
        mParkingSpaces = parkingSpaces;
        sTotalOffices++;
    }
	
    /**
     * Get the name of the business in the office.
     */
    public String getBusinessName() {
        return mBusinessName;
    }
	
    /**
     * Get the number of parking spaces.
     */
    public int getParkingSpaces() {
        return mParkingSpaces;
    }
	
    /**
     * Set the name of the business in the office.
     */
    public void setBusinessName(String businessName) {
        mBusinessName = businessName;
    }

    /**
     * Set the number of parking spaces.
     */
    public void setParkingSpaces(int parkingSpaces) {
        mParkingSpaces = parkingSpaces;
    }
		
    /**
     * Return a string representation of the business.
     */
    public String toString() {
        String string = "Business: ";
		
        if (mBusinessName != null) 
            string += mBusinessName;
        else 
            string += "unoccupied";
		
        if (mParkingSpaces > 0 ) 
            string += "; has " 
                   + mParkingSpaces 
                   + " parking spaces";

        string += " (total offices: " + sTotalOffices + ")";
		
        return string;
    }

    /**
     * Compare this object with the @a other object for equality.
     */
    public boolean equals(Object other) {
        boolean result = false;
		
        if (other instanceof Office) {
            final Office office = (Office) other;
			
            if (calcBuildingArea() == office.calcBuildingArea() 
                && getParkingSpaces() == office.getParkingSpaces()){
                result = true;
            }
        }
		
        return result;
    }
    // ASSIGNMENT TO-DO: DELETE TO END OF CLASS

}
