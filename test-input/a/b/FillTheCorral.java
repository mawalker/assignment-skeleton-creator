package mooc.vandy.java4android.gate.logic;

import java.util.Random;

import mooc.vandy.java4android.gate.ui.OutputInterface;

/**
 * This class uses your Gate class to fill the corral with snails.  We
 * have supplied you will the code necessary to execute as an app.
 * You must fill in the missing logic below.
 */
public class FillTheCorral {
    /**
     * Reference to the OutputInterface.
     */
    private OutputInterface mOut;

    /**
     * Constructor initializes the field.
     */
    FillTheCorral(OutputInterface out) {
        mOut = out;
    }

    // TODO -- Fill your code in here
    // ASSIGNMENT TO-DO -- Delete rest of class

    /**
     * This method takes in an array of Gates and a Random object and
     * sets the direction of each gate’s swing.  When Gate objects are
     * created, they are originally set to CLOSED.  Randomly set
     * each gate to swing IN, OUT, or leave it CLOSED.
     */
    public void setCorralGates(Gate[] gate, 
                               Random selectDirection){
        mOut.println("Initial gate setup:");

        // Iterate through all the gates and set their direction.
        for (int i = 0; i < gate.length; i++) {
            // Generates -1, 0, or 1.
            int direction =
                selectDirection.nextInt(3) - 1;

            if (direction != 0) 
                // Set to IN or OUT, otherwise leave as CLOSED.
                gate[i].open(direction);

            mOut.println("Gate " 
                         + i
                         + ": " 
                         + gate[i]);
        }
    }

    /**
     * Once the gates are randomly set, they must be checked to insure
     * that at least one is set to swing IN so that the snails can
     * enter at least one pen.  This method returns a boolean value of
     * false if all gates are set to OUT or CLOSED indicating we must
     * try to set them again.
     */
    public boolean anyCorralAvailable(Gate[] corral){
        // Iterate through all the gates and see if any corral is
        // available to let snails into.
        for (int i = 0; i < corral.length; i++)
            if (corral[i].getSwingDirection() == Gate.IN)
                // Find at least one gate allowing snails in.
                return true;

        // All gates are set to OUT or CLOSED.
        return false;
    }

    /**
     * Corrals all the snails, as follows:
     *     While there are snails still out to pasture
     *         Randomly select a gate.
     *         Randomly select a number of snails between 1 and total
     *             that are still out to pasture.
     * 	       Attempt to move this number of snails through the
     *             chosen gate.
     *	       If the chosen gate was set to swing IN to a corral,
     *             the number out to pasture is reduced.
     *	       If the chosen gate was set to swing OUT, the same
     *             number of snails actually gets out to pasture,
     *             increasing the number that are now out.
     *
     *	Returns how many tries were needed to get all of the snails
     *	into the corrals.
     */
    public int corralSnails(Gate[] corral,
                            Random rand) {
        int inPasture = 5;
        int tries = 0;
        int n, snails;

        // Iterate through all the gates try to corral the snails.
        for (; 
             inPasture > 0;
             tries++) {

            // Select a gate in the range of 0 thru 3 (inclusive).
            n = rand.nextInt(4); 

            // Select a number of snails to move
            snails = rand.nextInt(inPasture) + 1; 

            mOut.println(snails 
                         + " are trying to move through corral " 
                         + n);
            // Decrease the number of snails in the corral.
            inPasture -= corral[n].thru(snails);
        }

        mOut.println("It took " 
                     + tries 
                     + " attempts to corral all of the snails.");
        return tries;
    }
    // ASSIGNMENT TO-DO -- Delete rest of class

}
