package mooc.vandy.java4android.gate.logic;

/**
 * This file defines the Gate class.
 */
public class Gate {
	
    // TODO -- Fill in your code here

    // ASSIGNMENT TO-DO Delete the rest of class

    /**
     * IN swings the gate to let the snails move from the pasture into
     * contained area.
     */
    public static final int IN = 1;

    /**
     * OUT swings the gate to let the snails out of contained area and
     * into pasture.
     */
    public static final int OUT = -1;

    /**
     * CLOSED swings the gate closed which prevents movement in or out
     * of the pasture.
     */
    public static final int CLOSED = 0;

    /**
     * Stores the current state of the gate.  OUT (-1) swings to let
     * the snails out of contained area and into pasture.  IN (1)
     * swings to let the snails move from the pasture into contained
     * area. CLOSED (0) prevents any movement between the pen and
     * pasture.
     */
    private int mSwing;

    /**
     * Constructor initializes the field to the CLOSED position.
     */
    public Gate() {
        mSwing = CLOSED;
    }

    /**
     * Convenience method to check if the gate is in a CLOSED state.
     */
    public boolean isClosed() {
        return mSwing == CLOSED;
    }

    /**
     * Get method for mSwing field.
     */
    public int getSwingDirection() {
        return mSwing;
    }

    /**
     * Set method for swing, checks for valid input and returns true
     * if it's valid input, else false.
     */
    public boolean setSwing(int direction) {
        switch (direction) {
            case IN:
            case OUT:
            case CLOSED:
                mSwing = direction;
                return true;
            default:
                return false;
        }
    }

    /**
     * Set the direction of the gate to be IN or OUT.
     *
     * @param direction Either IN or OUT
     * @return true if the gate was opened, false if not.
     */
    public boolean open(int direction) {
        if (direction == IN || direction == OUT) {
            setSwing(direction);
            return true;
        } else
            return false;
    }

    /**
     * Close the gate.
     */
    public void close() {
        setSwing(CLOSED);
    }

    /**
     * You send as a parameter the number of “sheep” on the move.  If
     * the gate is set to swing out, return that value as a negative
     * number.  If the gate is set to swing in, return that value as a
     * positive number.  If the caller decides to use the return value
     * (they may not need it) it can be used to update counts for
     * example.
     */
    public int thru(int count) {
        if (getSwingDirection() != CLOSED) {
            if (mSwing == OUT) // They are leaving the pen.
                return count * -1;
            else // They are coming into the pen.
                return count;
        }
        else // Pen is closed.
            return 0;
    }

    /**
     * Return the Gate's state as a string (copy/pasted from PDF text).
     */
    public String toString() {
        String response;
        switch (getSwingDirection()) {
            case IN:
                response = "This gate is open and swings to enter the pen only";
                break;
            case OUT:
                response = "This gate is open and swings to exit the pen only";
                break;
            case CLOSED:
                response = "This gate is closed";
                break;
            default:
                response = "This gate has an invalid swing direction";
        }
        return response;
    }
    // ASSIGNMENT TO-DO Delete the rest of class

}
