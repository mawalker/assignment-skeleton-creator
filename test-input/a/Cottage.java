package mooc.vandy.java4android.buildings.logic;

/**
 * This is the cottage class file.  It is a subclass of House.
 */
public class Cottage
       extends House {

    // TODO - Put your code here.

    // ASSIGNMENT TO-DO: DELETE ALL LINES TO END OF CLASS

    /**
     * Is there a second floor?
     */
    private boolean mSecondFloor;

    /**
     * Constructor initializes the superclass.
     */
    public Cottage(int dimension,
                   int lotLength,
                   int lotWidth){
        super(dimension,
              dimension,
              lotLength,
              lotWidth);
    }

    /**
     * Constructor initializes the superclass and field to indicate
     * whether there's a second floor or not.
     */
    public Cottage(int dimension,
                   int lotLength,
                   int lotWidth,
                   String owner,
                   boolean second){
        super(dimension,
              dimension,
              lotLength,
              lotWidth,
              owner);
        mSecondFloor = second;
    }

    /**
     * Return true if the cottage has a second floor.
     */
    public boolean hasSecondFloor() {
        return mSecondFloor;
    }

    /**
     * Return a string representation of this object.
     */
    public String toString() {
        String output = super.toString() + "; is a ";
        if (mSecondFloor)
            output += "two story ";

        output +="cottage";

        return output;
    }
    // ASSIGNMENT TO-DO: DELETE ALL LINES TO END OF CLASS

}

