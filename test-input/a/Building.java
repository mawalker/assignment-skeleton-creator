package mooc.vandy.java4android.buildings.logic;

/**
 * This is the Building class file.
 */
public class Building {

    // TODO - Put your code here.

    // ASSIGNMENT TO-DO: DELETE TO END OF CLASS

    /**
     * Length of the building.
     */
    private int mLength;

    /**
     * Width of the building.
     */
    private int mWidth;

    /**
     * Length of the lot.
     */
    private int mLotLength;

    /**
     * Width of the lot.
     */
    private int mLotWidth;
	
    /**
     * Constructor initializes the fields.
     */
    public Building(int length,
                    int width,
                    int lotLength,
                    int lotWidth) {
        mLength = length;
        mWidth = width;
        mLotLength = lotLength;
        mLotWidth = lotWidth;
    }

    /**
     * Get the length of the building.
     */
    public int getLength() {
        return mLength;
    }
	
    /**
     * Get the width of the building.
     */
    public int getWidth() {
        return mWidth;
    }
	
    /**
     * Get the length of the lot.
     */
    public int getLotLength() {
        return mLotLength;
    }
	
    /**
     * Get the width of the lot.
     */
    public int getLotWidth() {
        return mLotWidth;
    }
	
    /**
     * Set the length of the building.
     */
    public void setLength(int length) {
        mLength = length;
    }

    /**
     * Set the width of the building.
     */
    public void setWidth(int width) {
        mWidth = width;
    }

    /**
     * Set the length of the lot.
     */
    public void setLotLength(int lotLength) {
        mLotLength = lotLength;
    }

    /**
     * Set the width of the lot.
     */
    public void setLotWidth(int lotWidth) {
        mLotWidth = lotWidth;
    }
	
    /**
     * Calculate the area of the building.
     */ 
    public int calcBuildingArea() {
        return mLength * mWidth;
    }
	
    /**
     * Calculate the area of the lot.
     */ 
    public int calcLotArea() {
        return mLotLength * mLotWidth;
    }

    /**
     * Return a string representation of the object.
     */
    public String toString() {
        return "Building: " + getLength() 
                            + " x " 
                            + getWidth() 
                            + " / "
                            + "Lot: " 
                            + getLotLength() 
                            + " x " 
                            + getLotWidth();
    }
    // ASSIGNMENT TO-DO: DELETE TO END OF CLASS

}
