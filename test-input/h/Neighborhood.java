package mooc.vandy.java4android.buildings.logic;

import java.io.FileNotFoundException;

import mooc.vandy.java4android.buildings.ui.OutputInterface;

/**
 * This Neighborhood utility class provides static helper methods the
 * print a Building List and calculate the area of a Building list.
 * A utility class in Java should always be final and have a private
 * constructor, as per https://en.wikipedia.org/wiki/Utility_class.
 */
public final class Neighborhood {

    // TODO - Put your code here.
    // ASSIGNMENT TO-DO: DELETE TO END OF CLASS
    
    /**
     * This helper method prints the buildings in your list.
     */
    public static void print(Building[] list, 
                             String header,
                             OutputInterface out) {
        out.println(header);
        out.println("----------");
		
        // Print all the buildings.
        for (Building item : list) 
            out.println("" + item);
    }
	
    /**
     * This method calculates the total square footage of all of the
     * lots in your Building List.
     */
    public static int calcArea(Building[] list) {
        int total = 0;

        // Calculate the area of all the buildings.		
        for (Building item : list) 
            total += item.calcLotArea();
		
        return total;
    }

    /**
     * Ensures this class is a utility class.
     */
    private Neighborhood() {}
    // ASSIGNMENT TO-DO: DELETE TO END OF CLASS

}
