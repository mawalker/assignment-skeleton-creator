package mooc.vandy.java4android.gate.logic;

import java.util.Random;

import mooc.vandy.java4android.gate.ui.OutputInterface;

/**
 * This class uses your Gate class to manage a herd of snails.  We
 * have supplied you will the code necessary to execute as an app.
 * You must fill in the missing logic below.
 */
public class HerdManager {
    /**
     * Reference to the output.
     */
    private OutputInterface mOut;

    /**
     * The input Gate object.
     */
    private Gate mEastGate;

    /**
     * The output Gate object.
     */
    private Gate mWestGate;

    /**
     * Maximum number of iterations to run the simulation.
     */
    private static final int MAX_ITERATIONS = 10;

    /**
     * Constructor initializes the fields.
     */
    public HerdManager(OutputInterface out,
                       Gate westGate,
                       Gate eastGate) {
        mOut = out;

        mWestGate = westGate;
        mWestGate.open(Gate.IN);

        mEastGate = eastGate;
        mEastGate.open(Gate.OUT);
    }

    // TODO -- Fill your code in here

    // ASSIGNMENT TO-DO -- Delete until end of class

    /**
     * This is the number of snails in your Herd
     */
    public static final int HERD = 24;

    /**
     * Begin the simulation with your herd in the pen.  For 10
     * iterations, move snails through gates and report on the totals.
     * If there is no snail in the pen, move a random number of snails
     * from pasture to pen.  If there is no snail in the pasture, move
     * a random number of snails from pen to pasture.  Otherwise,
     * randomly select a gate to move the random number of snails
     * through.  Report the results.
     */
    public void simulateHerd(Random rand) {
        int inPen = HERD;
        int inPasture = 0;

        String result = "There are currently "
            + inPen
            + " snails in the pen and "
            + inPasture
            + " snails in the pasture\n";

        for (int i = 0; i < MAX_ITERATIONS; i++) {
            if (inPen == 0) // Pen is empty, snails must return.
                inPen = inPen + mWestGate.thru(rand.nextInt(inPasture) + 1);
            else if (inPasture == 0) // Pen is full, snails must leave.
                inPen = inPen + mEastGate.thru(rand.nextInt(inPen) + 1);
                // Snails could come or go.
            else if (rand.nextBoolean()) // leave
                inPen = inPen + mEastGate.thru(rand.nextInt(inPen) + 1);
            else // return
                inPen = inPen + mWestGate.thru(rand.nextInt(inPasture) + 1);

            inPasture = HERD - inPen;

            result += "There are currently " 
                   + inPen 
                   + " snails in the pen and "
                   + inPasture
                   + " snails in the pasture\n";
        }

        mOut.println(result);
    }
    // ASSIGNMENT TO-DO -- Delete until end of class

}
