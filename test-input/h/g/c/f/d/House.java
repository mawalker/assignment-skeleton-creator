package mooc.vandy.java4android.buildings.logic;

/**
 * This is the House class file that extends Building.
 */
public class House
       extends Building {

    // TODO - Put your code here.
    // ASSIGNMENT TO-DO: DELETE TO END OF CLASS

    /**
     * Name of the owner of the house.
     */
    private String mOwner;

    /**
     * Keeps track of whether this house has a pool.
     */
    private boolean mPool;

    /**
     * Constructor initializes the fields.
     */
    public House(int length,
                 int width,
                 int lotLength,
                 int lotWidth) {
        super(length,
              width,
              lotLength,
              lotWidth);
        mOwner = null;
        mPool = false;
    }

    /**
     * Constructor initializes the fields and sets the owner of the
     * House.
     */
    public House(int length,
                 int width,
                 int lotLength,
                 int lotWidth,
                 String owner) {
        super(length,
              width,
              lotLength,
              lotWidth);
        mOwner = owner;
    }

    /**
     * Constructor initializes the fields and sets whether there's a
     * pool or not.
     */
    public House(int length,
                 int width,
                 int lotLength,
                 int lotWidth,
                 String owner,
                 boolean pool) {
        this(length,
             width,
             lotLength,
             lotWidth,
             owner);
        mPool = pool;
    }

    /**
     * Get the name of the house owner.
     */
    public String getOwner() {
        return mOwner;
    }

    /**
     * Set the name of the house owner.
     */
    public void setOwner(String owner) {
        mOwner = owner;
    }

    /**
     * Return true if the house has a pool, else false.
     */
    public boolean hasPool() {
        return mPool;
    }

    /**
     * Set whether the house has a pool.
     */
    public void setPool(boolean pool) {
        mPool = pool;
    }

    /**
     * Return a string representation of the object.
     */
    public String toString() {
        String string = "Owner: ";

        if (getOwner() != null)
            string += getOwner();
        else
            string += "n/a";

        if (hasPool())
            string += "; has a pool";

        final int buildingArea =
            calcBuildingArea();
        final int landArea =
            calcLotArea() - calcBuildingArea();

        if (landArea > buildingArea)
            string += "; has a big open space";

        return string;
    }

    /**
     * Compare this object for equality with the @a other object.
     */
    public boolean equals(Object other) {
        boolean result = false;

        if (other instanceof House) {
            final House house = (House) other;

            if (calcBuildingArea() == house.calcBuildingArea()
                && hasPool() == house.hasPool())
                result = true;
        }

        return result;
    }
    // ASSIGNMENT TO-DO: DELETE TO END OF CLASS

}
